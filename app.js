const express = require('express')
const app = express()
const port = 3000

/**
 * This Fun adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @return {Number}
 */
const add = (a,b) => {
    return a+b
}

app.get('/add/', (req, res) => {
    const x = add(1,2)
    res.send('Sum is '+x)
})
//usage ?a=X&b=Y
app.get('/dadd', (req, res) => {
    const a = parseInt(req.query.a)
    const b = parseInt(req.query.b)

    if(isNaN(a)|| isNaN(b)){
        res.send("Invalid parameters")
    }
    else{
        res.send("Sum is "+add(a,b))
    }
})
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})