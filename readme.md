# Node Task

Hello world express project

## Install steps
1. Download repo
2. run npm install

## Usage

code block
```sh
cd ./node-task
npm start
browse to localhost:3000 for hello world
browse to localhost:3000/dadd/?a=x&b=y, where x and y are numbers. Page returns the sum of a and b
```

Start cmd : `npm start`

## App should work

![Get distracted, buddy](https://media.tenor.com/images/1621bcc91852ffe700639d8c0a0a53d0/tenor.gif)